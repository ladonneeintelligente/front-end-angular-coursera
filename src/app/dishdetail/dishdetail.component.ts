import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { visibility, expand } from '../animations/app.animations';
@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  animations: [
    visibility(),
    expand()
  ]
})

export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishIds: number[];
  prev: number;
  next: number;
  errMess: string;
  dishcopy: Dish;
  visibility = 'shown';

  commentForm: FormGroup;
  new_comment: Comment;
  @ViewChild('cform') commentFormDirective;

  formErrors = {'author': '', 'comment': '' };
  validationMessages = {
    'author': {
      'required': 'This field is required',
      'minlength': 'Your name should be at least 2 characters long'
    },
    'comment': {
      'required': 'This field is required'
    }
  };

  constructor(  private dishservice: DishService,
                private route: ActivatedRoute,
                private location: Location,
                private fb: FormBuilder,
                @Inject('BaseURL') private BaseURL) {
                  this.createForm();
                }

  ngOnInit() {
    this.dishservice.getDishIds()
      .subscribe((dishIds) => this.dishIds = dishIds);
    this.route.params
      .pipe(switchMap((params: Params) => { this.visibility = 'hidden' ; return this.dishservice.getDish(+params['id']); }))
      .subscribe(
        dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'show'; },
        errmess => this.errMess = <any>errmess
      );
  }

  setPrevNext(dishId: number) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  createForm() {
    this.commentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2)]],
      rating: 5,
      comment: ['', Validators.required]
    });

    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.commentForm) {return; }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.new_comment = this.commentForm.value;
    const d = new Date();
    const comment = {
      'author': this.new_comment['author'],
      'rating': +this.new_comment['rating'],
      'comment': this.new_comment['comment'],
      'date': d.toISOString()
    };
    this.dishcopy.comments.push(comment);
    this.dishservice.putDish(this.dishcopy)
      .subscribe(
        dish => {this.dish = dish; this.dishcopy = dish;},
        errmess => {this.dish = null; this.dishcopy = null; this.BaseURL.errMess = <any>errmess ;}
      )
    console.log(this.new_comment);
    this.commentFormDirective.resetForm();
    this.commentForm.reset({
      author: '',
      rating: 5,
      comment: ''
    });
  }

}
